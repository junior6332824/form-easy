$(document).ready(function(){
    $('#btn_submit').click(function(){
        // собираем данные с формы
        var sed_name    = $('#sed_name').val();
        var sed_email   = $('#sed_email').val();
        var sed_comment = $('#sed_comment').val();
        // отправляем данные
        $.ajax({
            url: "action.php", // куда отправляем
            type: "post", // метод передачи
            dataType: "json", // тип передачи данных
            data: { // что отправляем
                "sed_name":    sed_name,
                "sed_email":   sed_email,
                "sed_comment": sed_comment
            },
            // после получения ответа сервера
            success: function(data){
                $('.messages').html(data.result); // выводим ответ сервера
            }
        });
    });
});